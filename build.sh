#!/usr/bin/env bash

npm install && bower install && polymer build --add-service-worker --bundle && docker build -t alejandroaranda/frontend .
