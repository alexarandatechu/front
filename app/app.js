(function(window) {
    var ROUTER_HOOKS = getRouterHooks();
    var ROUTES = getRoutes();
    var DEFAULT_ROUTE = ROUTES[0].name;

    Cells.config({
        pageRender: {
            mainNode: '#app__container',
            componentPath: 'app/pages',
            progressive: true,
            maxSimultaneousPages: 6
        },
        router: {
            useHash: true,
            routes: ROUTES,
            defaultRoute: DEFAULT_ROUTE,
            hooks: ROUTER_HOOKS
        }
    });

    function getRouterHooks() {
        var transitionChannel = Cells.Bus.channel('transition');
        var pageTitleChannel = Cells.Bus.channel('page_title');
        var errorChannel = Cells.Bus.channel('error');
        var pageChannel = Cells.Bus.channel('page');

        return {
            onTransitionStart: function(toState, fromState) {
                transitionChannel.publish('onTransitionStart');
                pageTitleChannel.publish('Starting Transitioning to...' + toState.name);
            },
            onTransitionActivate: function(toState, fromState) {
                transitionChannel.publish('onTransitionActivate');
                pageTitleChannel.publish('Activating transition to...' + toState.name);

                return Promise.resolve({});
            },
            onTransitionResolve: function(toState, fromState) {
                transitionChannel.publish('onTransitionResolve');
                pageTitleChannel.publish('Resolving transition to...' + toState.name);

                return Promise.resolve({});
            },
            onTransitionCancel: function(toState, fromState) {
                transitionChannel.publish('onTransitionCancel');
            },
            onTransitionError: function(toState, fromState, err) {
                transitionChannel.publish('onTransitionError');

                if (err && err.description) {
                    errorChannel.publish(err.description);
                }
            },
            onTransitionSuccess: function(toState, fromState) {
                transitionChannel.publish('onTransitionSuccess');
                pageTitleChannel.publish(toState.name);
                pageChannel.publish({name: toState.name + "-page", path: toState.path});
            }
        };
    }

    function getRoutes() {
        var ROUTE_HOOKS = getRouteHooks();
        return [
            createRoute('login', '/', {}, {}),
            createRoute('signup', '/signup', {}, {}),
            createRoute('home', '/home', {private: true}, ROUTE_HOOKS),
            createRoute('accounts', '/accounts', {private: true}, ROUTE_HOOKS),
            createRoute('newaccount', '/accounts/new', {private: true}, ROUTE_HOOKS),
            createRoute('editaccount', '/accounts/edit/:accountid', {private: true}, ROUTE_HOOKS),
            createRoute('movements', '/accounts/:accountid/movements', {private: true}, ROUTE_HOOKS),
            createRoute('newmovement', '/accounts/:accountid/movements/new', {private: true}, ROUTE_HOOKS),
            createRoute('movement', '/accounts/:accountid/movements/:movementid', {private: true}, ROUTE_HOOKS),
            createRoute('reports', '/reports', {private: true}, ROUTE_HOOKS),
            createRoute('forecasts', '/forecasts', {private: true}, ROUTE_HOOKS),
        ];
    }

    function getRouteHooks() {
        return {
            onActivate: function(toState, fromState, done) {
                var route = ROUTES.find(route => route.name === toState.name);
                var isPrivate = isPrivateRoute(route);
                var userLogged = isUserLogged();

                return new Promise((resolve, reject) => {
                    if (!isPrivate && userLogged) {
                        // Logout user if navigates to a public page
                        reject({redirect: { name: 'home' }});
                    } else {
                        if (!isPrivate || isPrivate && userLogged) {
                            resolve();
                        } else {
                            console.log("User not logged");
                            reject({redirect: { name: 'login' }});
                        }
                    }
                });
            },
            onResolve: function(toState, fromState, done) {
                return Promise.resolve({timestamp: new Date().toString()});
            }
        };
    }

    function createRoute(name, path, data, hooks) {
        return Object.assign({ name, path, data }, hooks);
    }

    function isPrivateRoute(state) {
        return state && state.data && state.data.private || false;
    }

    function isUserLogged() {
        var isLoggedValue = window.sessionStorage.getItem('isLogged') == "true" || false;

        return isLoggedValue;
    }
}(window));
